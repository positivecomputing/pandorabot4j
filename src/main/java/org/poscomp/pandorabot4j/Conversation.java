package org.poscomp.pandorabot4j;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by dnmilne on 13/11/2015.
 */
public class Conversation {

    private static final Logger logger = LoggerFactory.getLogger(Conversation.class) ;

    private static final String url = "http://www.pandorabots.com/pandora/talk-xml" ;

    private String botId ;
    private String customerId ;
    private Serializer serializer = new Persister();

    public Conversation(String botId) {
        this.botId = botId ;
    }

    public String sendMessage(String message) throws PandorabotException {

        HttpResponse<String> response = null;

        try {
            response = Unirest.post(url)
                    .field("botid", botId)
                    .field("custId", customerId)
                    .field("input", message)
                    .asString();
        } catch (UnirestException e) {
            throw new PandorabotException("Could not make request", e) ;
        }

        if (response.getCode() != 200)
            throw new PandorabotException("Pandorabot server returned error code " + response.getCode()) ;

        logger.debug(response.getBody()) ;


        Result result = null;
        try {
            result = serializer.read(Result.class, response.getBody());
        } catch (Exception e) {
            throw new PandorabotException("Could not parse response from pandorabot", e) ;
        }

        if (customerId == null)
            customerId = result.getCustId() ;

        return result.getThat() ;
    }





    private static class Result {

        @Attribute
        private int status ;

        @Attribute
        private String botid ;

        @Attribute
        private String custid ;

        @Element
        private String input ;

        @Element
        private String that ;

        public int getStatus() {
            return status;
        }

        public String getBotId() {
            return botid;
        }

        public String getCustId() {
            return custid;
        }

        public String getInput() {
            return input;
        }

        public String getThat() {
            return that;
        }
    }
}
