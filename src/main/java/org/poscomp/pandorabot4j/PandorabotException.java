package org.poscomp.pandorabot4j;

/**
 * Created by dnmilne on 13/11/2015.
 */
public class PandorabotException extends Exception {

    public PandorabotException() {
        super() ;
    }

    public PandorabotException(String message) {
        super(message) ;
    }

    public PandorabotException(String message, Throwable cause) {
        super(message, cause) ;
    }
}
