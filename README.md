Pandorabot4j is a lightweight Java API that provides programmatic access to [pandorabots](http://pandorabots.com/), so you can easily chat with one. 

##Limitations

This java client only lets you have a conversation with an existing pandorabot. It doesn't provide any functionality for building or modifying one. 

##Installation

To use this as a dependency within a Java Maven project, add the following to the `<repositories>` section 
in your project's `pom.xml` file.

    <repository>
        <id>poscomp-public</id>
        <name>Positive Computing Releases</name>
        <url>http://mvn.poscomp.org/content/repositories/releases</url>
    </repository>

Then add the following to the `<dependencies>` section

    <dependency>
      <groupId>org.poscomp</groupId>
      <artifactId>pandorabot4j</artifactId>
      <version>0.1.0</version>
    </dependency>

You should then be able to compile the code below.


##Usage

1. Create a **Conversation**, using the id of the bot you want to talk to.

    ```
    Conversation conversation = new Conversation(botid) ;
    ```

2. Send the bot a message, and get its reply

    ```
    String reply = conversation.send(message) ;
    ```

3. Repeat

Some good bots to talk to are:

* [Chompsky](http://demo.vhost.pandorabots.com/pandora/talk?botid=b0dafd24ee35a477)  who can look up answers on Wikipedia. `b0dafd24ee35a477`

* [Dr. Susan Calvin](http://demo.vhost.pandorabots.com/pandora/talk?botid=a91dc1affe345b93), a robopsychologist from Isaac Asimov's Robot Series. `a91dc1affe345b93`

* [Divabot](http://lauren.vhost.pandorabots.com/pandora/talk?botid=f6d4afd83e34564d) who acts like a diva, I guess? `f6d4afd83e34564d`