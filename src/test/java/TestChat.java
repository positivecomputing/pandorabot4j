import com.mashape.unirest.http.exceptions.UnirestException;
import org.junit.Test;
import org.poscomp.pandorabot4j.Conversation;
import org.poscomp.pandorabot4j.PandorabotException;

/**
 * Created by dnmilne on 13/11/2015.
 */
public class TestChat {

    @Test
    public void testChat() throws PandorabotException {

        Conversation conversation = new Conversation("b0dafd24ee35a477") ;

        String reply = conversation.sendMessage("hi!") ;

        System.out.println(reply) ;
    }
}
